# rip_ps1_disc_to_iso

This project houses a simple Bash shellscript that rips the contents of a PlayStation 1 game-disc to a .bin image file, also creating the required .cue metadata file, for use with a PlayStation emulator such as Beetle/Beetle HW.

**NOTE**: There are two dependencies that are required for this script to work:
    
    
*  cdrdao
*  bchunk

If you're using Debian GNU/Linux, the necessary packages can be found in the default repositories, and can be installed simply by running the following command from a Terminal window:

    sudo apt install cdrdao bchunk


**FINAL NOTE**: This project is not intended to be used for videogame piracy. I neither condone nor practice piracy in any way, shape, or form. Rather, I exercise my rights in accordance with US Copyright Law, specifically section 17 subsection 117. For more information, see the following links about US Copyright Law: Section 17 Subsection 117:

https://www.copyright.gov/help/faq/faq-digital.html

https://www.law.cornell.edu/uscode/text/17/117

