#!/bin/bash

# Static constants
PLAYSTATION_GAMES_DIR="${HOME}/Games/sony/ps1/games"

# Input variables
IMG_OUTPUT_NAME=${1}
MULTI_DISK_NUM=${2}

if [ -z "${IMG_OUTPUT_NAME}" ]; then
  echo
  echo "[ERROR]: Output file name required!"
  echo "[FORMAT]: ./rip_ps1_disc_to_iso.sh <output_name_sans_extension> <[disk_number_if_multi_disk_game]>"

  exit 1
fi

# Local variables
OUTPUT_DIR_NAME="${IMG_OUTPUT_NAME}"

if [ ! -d "${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME}" ]; then
  echo
  echo "[INFO]: Directory ${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME} does not exist. Creating..."

  mkdir -p "${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME}"
  RETURN_CODE=${?}

  if [ ${RETURN_CODE} -ne 0 ]; then
    echo
    echo "[ERROR]: Something went wrong with creation of ${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME}. Exiting..."

    exit ${RETURN_CODE}
  else
    echo
    echo "[SUCCESS]: Directory ${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME} created!"

    cd "${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME}"
  fi
fi

if [ -n "${MULTI_DISK_NUM}" ]; then
  IMG_OUTPUT_NAME="${IMG_OUTPUT_NAME}.disk_${MULTI_DISK_NUM}"

  # Create .m3u file for multi-disk games. Necessary for hot-swapping disks in-game.
  echo
  echo "${IMG_OUTPUT_NAME}.cue" >> "${PLAYSTATION_GAMES_DIR}/${OUTPUT_DIR_NAME}/${OUTPUT_DIR_NAME}.m3u"
fi

echo
echo "[INFO]: Creating ${IMG_OUTPUT_NAME}.bin..."

cdrdao read-cd                        \
  --read-raw                          \
  --datafile "${IMG_OUTPUT_NAME}.bin" \
  --device   "/dev/cdrom"             \
  --driver   "generic-mmc-raw"        \
  "${IMG_OUTPUT_NAME}.toc"
RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]; then
  echo
  echo "[ERROR]: Something went wrong in ${IMG_OUTPUT_NAME}.bin creation. Exiting..."

  exit ${RETURN_CODE}
else
  echo
  echo "[SUCCESS]: ${IMG_OUTPUT_NAME}.bin created!"
fi

echo
echo "[INFO]: Creating ${IMG_OUTPUT_NAME}.cue..."

toc2cue "${IMG_OUTPUT_NAME}.toc" "${IMG_OUTPUT_NAME}.cue"
RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]; then
  echo
  echo "[ERROR]: Something went wrong in ${IMG_OUTPUT_NAME}.cue creation. Exiting..."

  exit ${RETURN_CODE}
else
  echo
  echo "[SUCCESS]: ${IMG_OUTPUT_NAME}.bin and ${IMG_OUTPUT_NAME}.cue created!"
  echo
  echo "MD5 sum of ${IMG_OUTPUT_NAME}.bin: $(md5sum ${IMG_OUTPUT_NAME}.bin)"

  rm "${IMG_OUTPUT_NAME}.toc"

  exit 0
fi
